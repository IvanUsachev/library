package sourceit;

public class Generator {

    public static Literatura[] generateLiteratura() {
        Literatura[] literaturas = new Literatura[9];
        literaturas[0] = new Book("Книга","Тигр", "Пушкин", "Харьков", 2001);
        literaturas[1] = new Book("Книга","Осел", "Тургенев", "Киев", 2002);
        literaturas[2] = new Book("Книга","kk", "Тютчев", "Днепр", 2003);
        literaturas[3] = new Jurnal("Журнал","Зима", "Холодная", 2001, "январь");
        literaturas[4] = new Jurnal("Журнал","Осень", "Мокрая", 2002, "февраль");
        literaturas[5] = new Jurnal("Журнал","Лето", "Жаркое", 2003, "март");
        literaturas[6] = new Calendar("Календарь","Черный", "Сильная", "Харьков", 2001);
        literaturas[7] = new Calendar("Календарь","Красный", "Плохая", "Изюм", 2002);
        literaturas[8] = new Calendar("Календарь","Желтый", "Хорошая", "Чугуев", 2003);
        return literaturas;
    }
}


