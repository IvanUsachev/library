package sourceit;

public class Calendar extends Literatura{
    private String tema;
    private String izdatelstvo;


    public Calendar(String naimenovanie,String name, String tema, String izdatelstvo, int year) {
        super(naimenovanie,name, year);
        this.tema = tema;
        this.izdatelstvo = izdatelstvo;
     }
}
