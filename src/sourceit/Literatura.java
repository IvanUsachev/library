package sourceit;

import java.util.Objects;

public class Literatura {
    private String naimenovanie;
    private String name;
    private int year;

    public Literatura(String naimenovanie, String name, int year) {
        this.naimenovanie = naimenovanie;
        this.name = name;
        this.year = year;
    }

    public String getInfo() {
        return String.format("%s с названием: %s Год издания: %d год", naimenovanie, name, year);

    }

    public int getYear() {
        return year;
    }
}


