package sourceit;

public class Book extends Literatura {
    private String author;
    private String izdatelstvo;

    public Book(String naimenovanie, String name, String author, String izdatelstvo, int year) {
        super(naimenovanie, name, year);
        this.author = author;
        this.izdatelstvo = izdatelstvo;
    }
    public String getInfoBook() {
        return String.format("%s с названием: %s", author, izdatelstvo);

    }
}

