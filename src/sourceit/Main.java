package sourceit;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Literatura[] literaturas = Generator.generateLiteratura();
        Scanner in = new Scanner(System.in);
        System.out.println("Введите нужный год");
        int year = Integer.parseInt(in.nextLine());
        for (Literatura literatura : literaturas) {
            if (year == literatura.getYear()) {
                System.out.println(literatura.getInfo());
            } else {
                System.out.println("Под таким годом нет ничего");
                break;
            }
        }
    }
}



